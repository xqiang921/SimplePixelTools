#ifndef _INCLUDE_EVENTS_H_
#define _INCLUDE_EVENTS_H_
//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include <wx/event.h>
#include <wx/string.h>
#include <wx/image.h>

//=======================================================================//
//= Marco declare.                                                      =//
//=======================================================================//
#define EVT_PAINT_FONT(ID, FUNC)			EVT_COMMAND(ID, PaintFontUpdateEvent, FUNC)
#define EVT_GLOBAL_SETTINGS(ID, FUNC)		EVT_COMMAND(ID, GlobalSettingUpdateEvent, FUNC)

//=======================================================================//
//= Data type declare.                                                  =//
//=======================================================================//
wxDECLARE_EVENT(PaintFontUpdateEvent, wxCommandEvent);
wxDECLARE_EVENT(GlobalSettingUpdateEvent, wxCommandEvent);

//=======================================================================//
//= Class declare.                                                      =//
//=======================================================================//

#endif // _INCLUDE_EVENTS_H_
