#ifndef _INCLUDE_CLASS_PIXEL_CONVERTER_H_
#define _INCLUDE_CLASS_PIXEL_CONVERTER_H_

//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include <wx/dcmemory.h>
#include <wx/image.h>
#include <wx/bitmap.h>
#include <wx/font.h>
#include <wx/string.h>
#include "Settings.h"

//=======================================================================//
//= Class declare.                                                      =//
//=======================================================================//
class ImageRasterizer
: public wxMemoryDC
{
private:
    wxBitmap				m_clsPaintBuffer;
    wxPen					m_clsPaintPen;

protected:
	void					ResizePaintBuffer(const wxSize& clsNewSize);
	void					CleanPaintBuffer(void);
	void					DestroyPaintBuffer(void);

public:
							ImageRasterizer(const wxSize& clsSize = wxSize(16, 16));
							ImageRasterizer(const ImageRasterizer& clsSource);
							~ImageRasterizer(void);
	void					SetPixel(int iPosX, int iPosY, int iValue);
	int						GetPixel(int iPosX, int iPosY) const;
	int						GetWidth(void) const				{return m_clsPaintBuffer.GetWidth();}
	int						GetHeight(void) const				{return m_clsPaintBuffer.GetHeight();}
	const wxBitmap&			AsBitmap(void) const				{return m_clsPaintBuffer;}
	void					SetSize(const wxSize& clsSize);
};

class CharacterRasterizer
: public ImageRasterizer
{
private:
	wxChar					m_wcCharCode;
    FontSettings			m_clsFont;
	int						m_iIndex;
public:
							CharacterRasterizer(void);
							~CharacterRasterizer(void);
	virtual void			SetFont(const FontSettings& clsFont);
	void					Convert(const wxChar& wxCharCode);
};

#endif // _INCLUDE_CLASS_PIXEL_CONVERTER_H_
