#include <wx/datetime.h>
#include <wx/dir.h>
#include <wx/filename.h>
#include <fstream>
#include "Debug.h"

LogFile::LogFile(const wxString& cstrFileName)
: wxLog()
{
    /*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	wxFileName              clsLogFileName(cstrFileName);
	wxString                strLogPath;

	/*----------------------------------*/
	/* Initialize						*/
	/*----------------------------------*/
    strLogPath =            clsLogFileName.GetPath();

	/*----------------------------------*/
	/* Initialize						*/
	/*----------------------------------*/
    if(false == wxDir::Exists(strLogPath))
    {
        wxMkdir(strLogPath);
    }
    if(false == wxFile::Exists(cstrFileName))
    {
        m_clsLogFile.Create(cstrFileName);
    }
    m_clsLogFile.Open(cstrFileName, wxFile::write);
}

LogFile::~LogFile(void)
{
    if(true == m_clsLogFile.IsOpened())
    {
        m_clsLogFile.Flush();
        m_clsLogFile.Close();
    }
}

void LogFile::DoLogText(const wxString& cstrLogMsg)
{
    m_clsLogFile.Write(cstrLogMsg, wxConvUTF8);
}

void LogFile::DoLogTextAtLevel(wxLogLevel eLevel, const wxString & cstrLogMsg)
{
    if(true == m_clsLogFile.IsOpened())
    {
        switch(eLevel)
        {
            case wxLOG_FatalError:
            case wxLOG_Error:
            {
                m_clsLogFile.Write(wxT("[ERR]"));
                break;
            }
            case wxLOG_Warning:
            {
                m_clsLogFile.Write(wxT("[WRN]"));
                break;
            }
            case wxLOG_Message:
            {
                m_clsLogFile.Write(wxT("[MSG]"));
                break;
            }
            case wxLOG_Status:
            {
                m_clsLogFile.Write(wxT("[STA]"));
                break;
            }
            case wxLOG_Info:
            {
                m_clsLogFile.Write(wxT("[INF]"));
                break;
            }
            case wxLOG_Debug:
            {
                m_clsLogFile.Write(wxT("[DBG]"));
                break;
            }
            default:
            {
                m_clsLogFile.Write(wxT("[???]"));
            }
        }
        m_clsLogFile.Write(cstrLogMsg, wxConvUTF8);
        m_clsLogFile.Write(wxT("\n"));
    }
}
