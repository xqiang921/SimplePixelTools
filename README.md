# SimplePixelTools

#### 介绍
&emsp;&emsp;SimplePixelTools设计为一款针对单色显示屏的文字字模转换工具。
&emsp;&emsp;该软件作为SimpleGUI生态中的一个辅助工具，也可独立使用。
SimpleGUI的相关信息，请参照[SimpleGUI的开源中国主页](http://www.codeblocks.org/downloads/binaries)或[SimpleGUI的托管主页](https://gitee.com/Polarix/simplegui)

#### 功能
&emsp;&emsp;这款工具的编写灵感源自PCtoLCD2002这款软件，嵌入式圈子内的朋友，玩儿过点阵屏幕的，一定有接触过这款或类似这款的工具。   
><p align='center'><img src='Images/00/001.png' title='01-感谢并致敬PCtoLCD的开发者' style='max-width:1024px'></img></p>   
> - 感谢并致敬PCtoLCD的开发者，以及后来追加重制的PCtoLCD2018版本    

&emsp;&emsp;在使用PCtoLCD2002这款工具的过程中，我也发现了一些不足和不适合我自己使用习惯的地方，我以这些作为出发点，设计了这个文字和图像取模工具，在满足复刻PCtoLCD这款工具的前提下，针对我个人在实用工具时的一些需求，进行相应的改进开发。由于时作为SimpleGUI的一款衍生产物，所以取名为SimplePixelTools。工具目前提供文字取模功能和文本取模功能，图片取模功能目前还在开发中，敬请期待。
&emsp;&emsp;此工具作为SimpleGUI的周边工具之一，用于但不限于SimpleGUI，但由于本人能力有限，UI设计方面的功力上有很大欠缺，所以很多预想的功能还没有反映出来，软件的界面可能还会面临很大的调整，所以尚未正式发布，但是有具体需求的，可以尝试使用。如果有好的意见或建议，可以让我白嫖一些设计灵感，也欢迎提出。    
&emsp;&emsp;文本取模功能的输入来自于文本文件，可以对一个文本文件中的内容进行批量的取模处理，同时提供简单的去重复和排序功能。但是请注意，目前SimplePixelTools只能处理UTF-8格式的数据，排序也是按照Unicode编码的顺序进行排序，其他编码的处理还在适配中。

#### 编译和运行
&emsp;&emsp;此工具使用C++编写，基于wxWidgets构建，提供Codeblocks的编译工程。
&emsp;&emsp;编译环境的搭建与SimpleGUI的VirtualSDK环境搭建方法一致，请参照SimpleGUI的附加文档[配置Codeblocks下的wxWidgets开发环境](https://gitee.com/Polarix/simplegui/blob/Develope/Documents/A1-%E6%90%AD%E5%BB%BA%E5%9F%BA%E4%BA%8ECodeblocks%E7%9A%84wxWidgets%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83.md)

#### 使用说明

1. 文字取模模式
&emsp;&emsp;文字取模功能可以为一个或几个汉字取模，用于少量文字的加工和处理。    
><p align='center'><img src='Images/00/002.png' title='02-SimplePiselTools的文字取模模式' style='max-width:1024px'></img></p>   
> - SimplePiselTools的文字取模模式  

2. 文本取模模式
&emsp;&emsp;文本取模模式可以为大量文本进行取模操作，并将字模数据以C语言格式输出到文本文件。  
><p align='center'><img src='Images/00/003.png' title='03-SimplePiselTools的文本取模模式' style='max-width:1024px'></img></p>   
> - SimplePiselTools的文本取模模式  

&emsp;&emsp;相应的，如果变通一下，就可以为一个完整的字库进行取模，例如将一个字库的文字存储在一个文本文档内，并用SimplePixelTools的文本模式打开并取模。  
3. 图像取模模式
&emsp;&emsp;此模式下可以打开或编辑一张单色位图并转换成点阵模数据。   
><p align='center'><img src='Images/00/004.png' title='04-SimplePiselTools的图像取模模式' style='max-width:1024px'></img></p>   
> - SimplePiselTools的图像取模模式  

&emsp;&emsp;当然，编辑好的数据也可以以单色位图的文件形式保存，以备下次使用。

#### 联系开发者

&emsp;&emsp;首先，感谢您对SimplePixelTools的赏识与支持。
&emsp;&emsp;如果您有新的需求、提议亦或想法，欢迎在以下地址留言，或加入[QQ交流群799501887](https://jq.qq.com/?_wv=1027&k=5ahGPvK)留言交流。  
>SimplePixelTools@码云：https://gitee.com/Polarix/SimplePixelTools 

&emsp;&emsp;本人并不是全职的开源开发者，依然有工作及家庭的琐碎事务要处理，所以对于大家的需求和疑问反馈的可能并不及时，多有怠慢，敬请谅解。
&emsp;&emsp;最后，再次感谢您的支持。