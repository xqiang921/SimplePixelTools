///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Aug  8 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef _INCLUDE_CLASS_RESIZE_DIALOG_H_
#define _INCLUDE_CLASS_RESIZE_DIALOG_H_

//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include <wx/string.h>
#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/button.h>
#include <wx/spinctrl.h>
#include <wx/dialog.h>

//=======================================================================//
//= Class declare.                                                      =//
//=======================================================================//
class ResizeDialog : public wxDialog
{
	private:
		wxStaticText*			m_pclsText;
		wxSpinCtrl*				m_pclsWidthSpin;
		wxSpinCtrl*				m_pclsHeightSpin;
		wxButton*				m_pclsConfirmButton;
		wxButton*				m_pclsCancelButton;

		wxSize					m_clsSizeValue;

		void					_create(wxSizer* pclsRootSizer);
		wxStaticText*			_newStaticText(const wxString& cstrText);

	protected:

	public:

								ResizeDialog(	wxWindow* pclsParent, wxWindowID iID = wxID_ANY,
												const wxSize& clsSizeValue = wxSize(128, 64));
								~ResizeDialog(void);
		const wxSize&			GetSizeValue(void) const						{return m_clsSizeValue;}
		void					SetSizeValue(const wxSize& clsNewSize);

		int						ShowModal(void);
		int						ShowModal(const wxString& strNoticeText, const wxSize& clsSizeValue);
};

#endif //_INCLUDE_CLASS_RESIZE_DIALOG_H_
