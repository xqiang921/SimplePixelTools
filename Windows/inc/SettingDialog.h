#ifndef _INCLUDE_CLASS_SETTING_DIALOG_H_
#define _INCLUDE_CLASS_SETTING_DIALOG_H_

//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include <wx/dialog.h>
#include <wx/colour.h>
#include <wx/clrpicker.h>
#include <wx/bmpbuttn.h>
#include <wx/combobox.h>
#include <wx/stattext.h>
#include <wx/timer.h>
#include <wx/radiobut.h>
#include <wx/checkbox.h>

#include "wxLCDBase.h"
#include "Settings.h"
#include "ControlID.h"

//=======================================================================//
//= Marco declare.                                                      =//
//=======================================================================//
#define	PERVIEW_STEP_INTERVAL_MS	(30)

//=======================================================================//
//= Data type define.                                                   =//
//=======================================================================//
typedef struct
{
	int							iRowCount;
	int							iColumnCount;
	int							iRowIndex;
	int							iColumnIndex;
	int							iPaintPosX;
	int							iPaintPosY;
	int							iBitIndex;
}SETTING_PREVIEW_CTRL;

//=======================================================================//
//= Class declare.                                                      =//
//=======================================================================//
class SettingDialog : public wxDialog
{
	DECLARE_EVENT_TABLE();

	private:
		wxComboBox*				m_pclsConfigList;
		wxBitmapButton*			m_pclsRemoveCfgButton;
		wxBitmapButton*			m_pclsSaveCfgButton;
		wxColourPickerCtrl*		m_pclsPanelColourPicker;
		wxColourPickerCtrl*		m_pclsPixelColourPicker;
		wxColourPickerCtrl*		m_pclsGridColourPicker;
		wxChoice*				m_pclsWorkTypeChoice;
		wxChoice*				m_pclsDirectionChoice;
		wxChoice*				m_pclsBitOrderChoice;
		wxChoice*				m_pclsCommentChoice;
		wxCheckBox*				m_pclsCommentNewLineChk;

		wxLCDBase*				m_pclsPerview;
		wxButton*				m_pclsOKButton;
		wxButton*				m_pclsCancelButton;
		wxTimer*				m_pclsPerviewTimer;

		GlobalConfiguration		m_clsTempConfiguration;
		SETTING_PREVIEW_CTRL	m_stPreviewCtrl;

		void					_startPreview(void);
		void					_stopPerview(void);
		wxStaticText*			_createStaticText(const wxString& cstrText, wxWindowID iID = wxID_ANY);
		wxChoice*				_createSettingChoice(const wxString arrstrSettingItemList[], size_t sItemCount, wxWindowID iID = wxID_ANY, int iSelection = 0);
		wxChoice*				_createWorkTypeChoice(wxWindowID iID = wxID_ANY);
		wxChoice*				_createDirectionChoice(wxWindowID iID = wxID_ANY);
		wxChoice*				_createBitOrderChoice(wxWindowID iID = wxID_ANY);
		void                    _loadConfigFiles(void);

	protected:
		virtual void			OnClose(wxCloseEvent& clsEvent);
		virtual void			OnColourChanged(wxColourPickerEvent& clsEvent);
		virtual void			OnParamChanged(wxCommandEvent& clsEvent);
		virtual void			OnPerviewTimer(wxTimerEvent& clsEvent);
		virtual void            OnPersetSelected(wxCommandEvent& clsEvent);
		virtual void            OnSaveCfg(wxCommandEvent& clsEvent);
		virtual void            OnRemoveCfg(wxCommandEvent& clsEvent);
		virtual void			OnCommentSettingChanged(wxCommandEvent& clsEvent);

	public:

                                SettingDialog(wxWindow* pclsParent);
                                ~SettingDialog(void);
		virtual int				ShowModal(void);
		virtual void            SetConfiguration(const GlobalConfiguration& clsConfiguration);
        const GlobalConfiguration&	GetSettings(void) const;

};


#endif // _INCLUDE_CLASS_SETTING_DIALOG_H_
