#ifndef _INCLUDE_CLASS_IMAGE_TOOL_PANEL_H_
#define _INCLUDE_CLASS_IMAGE_TOOL_PANEL_H_

//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include <wx/frame.h>
#include <wx/toolbar.h>
#include <wx/scrolwin.h>
#include <wx/textctrl.h>
#include <wx/panel.h>
#include "PicturePaintCtrl.h"
#include "GeneralToolBar.h"
#include "Encoder.h"
#include "sptChildWindowBase.h"

//=======================================================================//
//= Global variable declare.										    =//
//=======================================================================//
extern const wxString PIXEL_IMAGE_WINDOW_NAME;

//=======================================================================//
//= Class declare.                                                      =//
//=======================================================================//
class ImageToolWindow
: public sptChildWindowBase
{
	DECLARE_EVENT_TABLE();
private:
    wxToolBar*				        m_pclsToolBar;
    wxScrolledWindow*		        m_pclsWorkingPanel;
    PicturePaintCtrl*		        m_pclsPicPaintCtrl;

    void							_create(void);
    wxToolBar*						_createToolsBar(void);
    void							_configPaintPantl(void);
protected:
	virtual void					OnFontChanged(wxCommandEvent& clsEvent);
	virtual void					OnSettingChanged(wxCommandEvent& clsEvent);
    virtual void					OnNewImage(wxCommandEvent& clsEvent);
    virtual void					OnOpenImage(wxCommandEvent& clsEvent);
    virtual void					OnResize(wxCommandEvent& clsEvent);

public:
                                    ImageToolWindow(wxWindow* pclsParent);
                                    ImageToolWindow(wxWindow* pclsParent, const wxSize& clsImageSize);
									ImageToolWindow(wxWindow* pclsParent, const wxString& cstrFilePath);
                                    ~ImageToolWindow(void);
    virtual bool					StartProcess(wxEvtHandler* pclsEventHandler);
    virtual bool					OpenFile(void);
    virtual bool					OpenFile(const wxString& cstrPath);
    virtual bool					SaveFile(void);
};


#endif // _INCLUDE_CLASS_IMAGE_TOOL_PANEL_H_
