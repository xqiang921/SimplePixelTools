#ifndef _INCLUDE_MDI_BASE_WINDOW_CLASS_H_
#define _INCLUDE_MDI_BASE_WINDOW_CLASS_H_
//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include <wx/string.h>
#include <wx/window.h>
#include <wx/thread.h>

//=======================================================================//
//= Data type declare.                                                  =//
//=======================================================================//

//=======================================================================//
//= Global variable declare.										    =//
//=======================================================================//

//=======================================================================//
//= Class declare.                                                      =//
//=======================================================================//
class sptChildWindowBase
: public wxWindow
{
private:
	wxWindow*						m_pclsParentFrame;
	wxString						m_strWindowTitle;
	wxString						m_strFilePath;

protected:
	void							SetFilePath(const wxString& cstrFilePath);

public:
                                    sptChildWindowBase(wxWindow* pclsParent, wxWindowID iID = wxID_ANY, const wxString& strFilePath = wxEmptyString);
                                    ~sptChildWindowBase(void);
	void							UpdateTitle(void);
	virtual void					SetTitle(const wxString& cstrNewTitle){m_strWindowTitle = cstrNewTitle;}
	virtual const wxString& 		GetTitle(void) const		{return m_strWindowTitle;}
	virtual wxWindow*				GetParent(void)				{return m_pclsParentFrame;}
	const wxString&					GetFilePath(void) const		{return m_strFilePath;}
	bool							PostEventToParent(wxEvent& clsEvent);
	virtual bool					OpenFile(void) = 0;
	virtual bool					OpenFile(const wxString& cstrPath) = 0;
	virtual bool					SaveFile(void) = 0;
	virtual bool					StartProcess(wxEvtHandler* pclsEventHandler) = 0;
	virtual bool					StopProcess(void);
};

#endif // _INCLUDE_MDI_BASE_WINDOW_CLASS_H_
