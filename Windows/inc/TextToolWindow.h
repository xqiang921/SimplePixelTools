#ifndef _INCLUDE_CLASS_TEXT_TOOL_WINDOW_H_
#define _INCLUDE_CLASS_TEXT_TOOL_WINDOW_H_

//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include <wx/string.h>
#include <wx/textctrl.h>
#include <wx/window.h>
#include "GlobalEvents.h"
#include "TextPaintCtrl.h"
#include "InputTextCtrl.h"
#include "sptChildWindowBase.h"

//=======================================================================//
//= Class declare.                                                      =//
//=======================================================================//
class TextToolWindow
: public sptChildWindowBase
{
    DECLARE_EVENT_TABLE();
private:
    InputTextCtrl*					m_pclsInputTextCtrl;
    TextPaintCtrl*					m_pclsPerviewPanel;
    wxWindow*						m_pclsParentFrame;
    bool							m_bIsChanged;

    void							_create(wxWindow* pclsParent);
    void							_updatePaintConfig(void);
    void							_resizePerviewPanel(int iFontSize);
    void							_paintText(const wxString& cstrText);


protected:
    virtual void					OnSizeChanged(wxSizeEvent& clsEvent);
    virtual void					OnTextChanged(wxCommandEvent& clsEvent);
    virtual void					OnTextSelectionChange(TextSelectEvent& clsEvent);
    virtual void					OnFontChanged(wxCommandEvent& clsEvent);
    virtual void					OnSettingChanged(wxCommandEvent& clsEvent);
    virtual void					OnWindowActived(wxActivateEvent& clsEvent);

public:
                                    TextToolWindow(wxWindow* pclsParent);
                                    TextToolWindow(wxWindow* pclsParent, const wxString& cstrFilePath);
                                    ~TextToolWindow(void);
    virtual wxString				GetInput(void) const;
    virtual bool					StartProcess(wxEvtHandler* pclsEventHandler);
    virtual bool					OpenFile(void);
    virtual bool					OpenFile(const wxString& cstrPath);
    virtual bool					SaveFile(void);
};

#endif // _INCLUDE_CLASS_TEXT_TOOL_WINDOW_H_
