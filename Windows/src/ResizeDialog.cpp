///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Aug  8 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////
//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include "ResizeDialog.h"

//=======================================================================//
//= Include files.													    =//
//=======================================================================//
ResizeDialog::ResizeDialog(wxWindow* pclsParent, wxWindowID iID, const wxSize& clsSizeValue)
: wxDialog(pclsParent, iID, wxT("Resize paint area."), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE)
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxBoxSizer* pclsRootSizer = new wxBoxSizer( wxVERTICAL );

	if(nullptr != pclsRootSizer)
	{
		_create(pclsRootSizer);
		pclsRootSizer->Fit(this);
	}

	SetSizeValue(clsSizeValue);

	Layout();
	Centre( wxBOTH );
}

ResizeDialog::~ResizeDialog()
{
}

void ResizeDialog::_create(wxSizer* pclsRootSizer)
{
	wxBoxSizer* pclsTextSizer = new wxBoxSizer(wxHORIZONTAL);
	wxBoxSizer* pclsInputSizer = new wxBoxSizer(wxHORIZONTAL);
	wxBoxSizer* pclsButtonSizer = new wxBoxSizer(wxHORIZONTAL);

	if(nullptr != pclsTextSizer)
	{
		m_pclsText = _newStaticText(wxT("Input a new size."));
		pclsTextSizer->Add( m_pclsText, 1, wxALL|wxEXPAND, 5 );
	}

	if(nullptr != pclsInputSizer)
	{
		pclsInputSizer->Add(_newStaticText(wxT("Width:")), 0, wxALIGN_CENTER_VERTICAL|wxTOP|wxBOTTOM|wxLEFT, 5);
		m_pclsWidthSpin = new wxSpinCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 100,-1 ), wxSP_ARROW_KEYS, 1, 320, 0);
		pclsInputSizer->Add( m_pclsWidthSpin, 0, wxALIGN_CENTER_VERTICAL|wxALL, 5 );

		pclsInputSizer->Add(_newStaticText(wxT("Height:")), 0, wxALIGN_CENTER_VERTICAL|wxTOP|wxBOTTOM|wxLEFT, 5);
		m_pclsHeightSpin = new wxSpinCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 100,-1 ), wxSP_ARROW_KEYS, 1, 320, 0);
		pclsInputSizer->Add( m_pclsHeightSpin, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	}

	if(nullptr != pclsButtonSizer)
	{
		m_pclsConfirmButton = new wxButton( this, wxID_OK, wxT("OK"), wxDefaultPosition, wxDefaultSize, 0);
		pclsButtonSizer->Add(m_pclsConfirmButton, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5);

		m_pclsCancelButton = new wxButton( this, wxID_CANCEL, wxT("Cancel"), wxDefaultPosition, wxDefaultSize, 0);
		pclsButtonSizer->Add(m_pclsCancelButton, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5);

		m_pclsConfirmButton->SetDefault();
	}

	pclsRootSizer->Add(pclsTextSizer, 0, wxEXPAND, 5);
	pclsRootSizer->Add(pclsInputSizer, 0, wxEXPAND, 5);
	pclsRootSizer->Add(pclsButtonSizer, 0, wxALIGN_CENTER_HORIZONTAL, 5);

	SetSizer( pclsRootSizer );
}

wxStaticText* ResizeDialog::_newStaticText(const wxString& cstrText)
{
	return new wxStaticText(this, wxID_ANY, cstrText, wxDefaultPosition, wxDefaultSize, 0);
}

void ResizeDialog::SetSizeValue(const wxSize& clsNewSize)
{
    m_clsSizeValue = clsNewSize;
    m_pclsWidthSpin->SetValue(clsNewSize.GetWidth());
    m_pclsHeightSpin->SetValue(clsNewSize.GetHeight());
}


int ResizeDialog::ShowModal(void)
{
	int		iDialogResult;

	iDialogResult = wxDialog::ShowModal();

	if(wxID_OK == iDialogResult)
	{
		m_clsSizeValue.SetWidth(m_pclsWidthSpin->GetValue());
		m_clsSizeValue.SetHeight(m_pclsHeightSpin->GetValue());
	}

	return iDialogResult;
}

int ResizeDialog::ShowModal(const wxString& strNoticeText, const wxSize& clsSizeValue)
{
	m_pclsText->SetLabel(strNoticeText);
	m_clsSizeValue = clsSizeValue;
	return ShowModal();
}
