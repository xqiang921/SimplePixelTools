//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include <wx/string.h>
#include <wx/filename.h>
#include "sptChildWindowBase.h"
#include "Utility.h"
#include "WorkThread.h"

//=======================================================================//
//= Global variable define.											    =//
//=======================================================================//
const wxString MDI_CHILD_BASE_WINDOW_NAME = wxT("MDI Child Window");
const wxString MDI_CHILD_DEFAULT_TITLE = wxT("New Creation");

//=======================================================================//
//= Function define.										            =//
//=======================================================================//
sptChildWindowBase::sptChildWindowBase(wxWindow* pclsParent, wxWindowID iID, const wxString& strFilePath)
: wxWindow(pclsParent, iID, wxDefaultPosition, pclsParent?pclsParent->GetClientSize():wxDefaultSize, wxBORDER_NONE, MDI_CHILD_BASE_WINDOW_NAME)
{
	m_pclsParentFrame = pclsParent;
	m_strWindowTitle = MDI_CHILD_DEFAULT_TITLE;
	if(wxFileName::Exists(strFilePath))
	{
		m_strFilePath = strFilePath;
	}
	else
	{
		m_strFilePath = wxEmptyString;
	}
	UpdateTitle();
}

sptChildWindowBase::~sptChildWindowBase(void)
{

}

void sptChildWindowBase::UpdateTitle(void)
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	wxString				strTitleText = wxEmptyString;
	wxFileName				clsFileName(m_strFilePath);

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	if(clsFileName.GetFullName().IsEmpty())
	{
		m_strWindowTitle = MDI_CHILD_DEFAULT_TITLE;
	}
	else
	{
		m_strWindowTitle = clsFileName.GetFullName();
	}
	SetTitle(m_strWindowTitle);
}

bool sptChildWindowBase::PostEventToParent(wxEvent& clsEvent)
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	wxEvtHandler*			pclsParent = m_pclsParentFrame;
	bool					bReturn;

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	if(pclsParent)
	{
		bReturn = pclsParent->ProcessEvent(clsEvent);
	}
	else
	{
		bReturn = false;
	}

	return bReturn;
}

bool sptChildWindowBase::StopProcess(void)
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	return WorkThreadMgr::Instance().StopThread();
}

void sptChildWindowBase::SetFilePath(const wxString& cstrFilePath)
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	m_strFilePath = cstrFilePath;
}
