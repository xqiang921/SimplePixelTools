#ifndef _INCLUDE_SPT_CLASS_ARROW_BUTTON_H_
#define _INCLUDE_SPT_CLASS_ARROW_BUTTON_H_
//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include <wx/event.h>
#include <wx/aui/auibook.h>
#include "sptChildWindowBase.h"

//=======================================================================//
//= Data type declare.                                                  =//
//=======================================================================//
typedef enum _e_work_type_
{
	WORK_TYPE_UNKNOWN = 0,
	WORK_TYPE_TEXT,
	WORK_TYPE_FONT,
	WORK_TYPE_IMAGE,
	WORK_TYPE_MAX
}WORK_TYPE;

//=======================================================================//
//= Class declare.                                                      =//
//=======================================================================//
class sptWindowMgr : public wxAuiNotebook
{
    wxDECLARE_CLASS(sptWindowMgr);
    wxDECLARE_NO_COPY_CLASS(sptWindowMgr);

//	DECLARE_EVENT_TABLE();

private:

public:
								sptWindowMgr(wxWindow* pclsParent, wxWindowID iWinID = wxID_ANY);
								~sptWindowMgr(void);
    virtual bool				AddWindow(sptChildWindowBase *pclsNewPage, bool bSelect = false, const wxBitmap& clsIcon = wxNullBitmap);
    virtual sptChildWindowBase*	GetWindow(size_t sPageIndex) const;
    virtual int					SelectWindow(size_t sPageIndex);
    virtual int					SelectWindow(sptChildWindowBase* pclsWindow);
    virtual sptChildWindowBase* GetSelectedWindow(void);
    virtual sptChildWindowBase* SearchOpenedFile(const wxString& cstrFilePath);
    virtual void				UpdateTitle(int iPageIndex);

};

#endif //_INCLUDE_CLASS_ARROW_BUTTON_H_
