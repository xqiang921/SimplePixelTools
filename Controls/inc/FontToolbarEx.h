#ifndef _INCLUDE_CLASS_FONT_TOOLS_BAR_EX_H_
#define _INCLUDE_CLASS_FONT_TOOLS_BAR_EX_H_
//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include "ToolbarBase.h"
#include <wx/combobox.h>
#include <wx/spinctrl.h>

enum FONT_TOOL_ID
{
    FONT_TOOL_EX_ID_HIGHEST = wxID_HIGHEST+4000,
    FONT_TOOL_EX_ID_FORCE_WIDTH,
    FONT_TOOL_EX_ID_FORCE_WIDTH_VALUE,
    FONT_TOOL_EX_ID_CUT_OUT,
    FONT_TOOL_EX_ID_CUT_OUT_HEIGHT,
    FONT_TOOL_EX_ID_OFFSET,
    FONT_TOOL_EX_ID_OFFSET_LEFT,
    FONT_TOOL_EX_ID_OFFSET_UP,
    FONT_TOOL_EX_ID_OFFSET_RIGHT,
    FONT_TOOL_EX_ID_OFFSET_DOWN,
    FONT_TOOL_EX_ID_OFFSET_RESET,
    FONT_TOOL_EX_ID_MAX
};

//=======================================================================//
//= Class declare.                                                      =//
//=======================================================================//
class FontToolBarEx : public ToolbarBase
{
    wxDECLARE_NO_COPY_CLASS(FontToolBarEx);
	wxDECLARE_EVENT_TABLE();
private:
	wxSpinCtrl*						m_pclsForceWidthSpin;
	wxSpinCtrl*						m_pclsCutoutHeightSpin;
	void							_loadSettings(void);
	void							_postPaintFontUpdateEvent(void);

protected:
	virtual void					OnToolEvent(wxCommandEvent& clsEvent);
	virtual void					OnSpinChanged(wxSpinEvent& clsEvent);

public:
									// Constructor/Destructor
									FontToolBarEx(wxWindow* pclsParent, wxWindowID iID = wxID_ANY);
									~FontToolBarEx(void);
	void							OnProcessStart(void);
	void							OnProcessStop(void);
};

#endif //_INCLUDE_CLASS_FONT_TOOLS_BAR_EX_H_
