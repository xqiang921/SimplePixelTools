#ifndef _INCLUDE_CLASS_TEXT_PAINT_CTRL_H_
#define _INCLUDE_CLASS_TEXT_PAINT_CTRL_H_

//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include <wx/log.h>
#include <wx/colour.h>
#include "wxLCDBase.h"
#include "Rasterizer.h"

//=======================================================================//
//= Class declare.                                                      =//
//=======================================================================//
class TextPaintCtrl: public wxLCDBase
{
	//DECLARE_EVENT_TABLE();

	private:
		wxColour					m_clsPixelHColour;
		wxColour					m_clsPixelLColour;
		CharacterRasterizer			m_clsTextConv;
		wxString					m_strText;

	protected:

	public:
									TextPaintCtrl(	wxWindow *pclsParent,
												wxWindowID iWinID = wxID_ANY,
												const wxSize& clsSizeInPixel = wxDefaultSizeInPixel,
												const wxColor& clsInitializeFillColor = *wxBLACK);
									~TextPaintCtrl(void);
		int							GetPixel(const int iPosX, const int iPosY);
		void						SetPixel(const int iPosX, const int iPosY, const int iValue);
		void						CleanScreen(void);
		void						SetPanelColour(const wxColour& clsPanelColour, bool bRefreshNow = true);
		void						SetPixelColour(const wxColour& clsPixelColour, bool bRefreshNow = true);
		void						PaintChar(int iPixelX, int iPixelY, const CharacterRasterizer& clsDCObject);
		const wxColour&				GetPixelHColour(void) const					{return m_clsPixelHColour;}
		const wxColour&				GetPixelLColour(void) const					{return m_clsPixelLColour;}
		void						AutoHorizontalPixelNumber(void);
		void						AutoVerticalPixelNumber(void);
		void						PaintText(const wxString& cstrText);
		void						SetFont(const FontSettings& clsFont);

};

#endif // _INCLUDE_CLASS_TEXT_PAINT_CTRL_H_
