#ifndef _INCLUDE_CLASS_POPUP_ARROW_BUTTON_H_
#define _INCLUDE_CLASS_POPUP_ARROW_BUTTON_H_

#include <wx/popupwin.h>
#include <wx/sizer.h>
#include <wx/bmpbuttn.h>

class PopupArrowButton : public wxPopupTransientWindow
{
	wxDECLARE_CLASS(PopupArrowButton);
	wxDECLARE_NO_COPY_CLASS(PopupArrowButton);

	DECLARE_EVENT_TABLE();

	private:
		int						m_iID;
		wxGridSizer*			m_pclsGridSizer_RootSizer;
		wxBitmapButton*			m_pclsBitmapButton_MoveUp;
		wxBitmapButton*			m_pclsBitmapButton_MoveLeft;
		wxBitmapButton*			m_pclsBitmapButton_ResetOffset;
		wxBitmapButton*			m_pclsBitmapButton_MoveRight;
		wxBitmapButton*			m_pclsBitmapButton_MoveDown;


		void					_addButton(wxWindow* pclsControls);
		void					_resize(void);

	protected:
		virtual void			OnButtonClick(wxCommandEvent& clsEvent);
		virtual void			OnKeyDown(wxKeyEvent& clsEvent);
		virtual void			OnLostFocus(wxFocusEvent& clsEvent);

	public:
    // Constructor/Destructor
								PopupArrowButton(wxWindow* pclsParent, wxWindowID iWinID = wxID_ANY);
								~PopupArrowButton(void);

};

#endif //_INCLUDE_CLASS_POPUP_ARROW_BUTTON_H_
