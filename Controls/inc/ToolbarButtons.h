#ifndef _INCLUDE_CLASS_TOOL_BUTTONS_H_
#define _INCLUDE_CLASS_TOOL_BUTTONS_H_

#include <wx/toolbar.h>

class TextWindowTool: public wxToolBarToolBase
{
	public:
		TextWindowTool(wxToolBarBase *pclsToolBar = nullptr, int iID = wxID_ANY)
		:wxToolBarToolBase(pclsToolBar, iID, wxT("Text Window"), wxBITMAP_PNG(RES_ID_TOOL_NEW_TEXT_WINDOW), wxNullBitmap,
							wxITEM_NORMAL, nullptr, wxT("Text window"), wxT("Open a new text window."))
		{

		}
};

class FontWindowTool: public wxToolBarToolBase
{
	public:
		FontWindowTool(wxToolBarBase *pclsToolBar = nullptr, int iID = wxID_ANY)
		:wxToolBarToolBase(pclsToolBar, iID, wxT("Font Window"), wxBITMAP_PNG(RES_ID_TOOL_NEW_FONT_WINDOW), wxNullBitmap,
							wxITEM_NORMAL, nullptr, wxT("Font window"), wxT("Open a new font window."))
		{

		}
};

class ImageWindowTool: public wxToolBarToolBase
{
	public:
		ImageWindowTool(wxToolBarBase *pclsToolBar = nullptr, int iID = wxID_ANY)
		:wxToolBarToolBase(pclsToolBar, iID, wxT("Image Window"), wxBITMAP_PNG(RES_ID_TOOL_NEW_IMAGE_WINDOW), wxNullBitmap,
							wxITEM_NORMAL, nullptr, wxT("Image window"), wxT("Open a new image window."))
		{

		}
};

class OpenFileTool: public wxToolBarToolBase
{
	public:
		OpenFileTool(wxToolBarBase *pclsToolBar = nullptr, int iID = wxID_ANY)
		:wxToolBarToolBase(pclsToolBar, iID, wxT("Open File"), wxBITMAP_PNG(RES_ID_TOOL_OPEN), wxNullBitmap,
							wxITEM_NORMAL, nullptr, wxT("Open a file"), wxT("Open a file."))
		{

		}
};

#endif // _INCLUDE_CLASS_TOOL_BUTTONS_H_
