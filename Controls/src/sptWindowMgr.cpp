//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include "sptWindowMgr.h"

//=======================================================================//
//= Data type define.                                                   =//
//=======================================================================//
wxIMPLEMENT_CLASS(sptWindowMgr, wxAuiNotebook);

//=======================================================================//
//= Event table.													    =//
//=======================================================================//
#if 0
BEGIN_EVENT_TABLE(sptWindowMgr, wxControl)

END_EVENT_TABLE()
#endif

sptWindowMgr::sptWindowMgr(wxWindow* pclsParent, wxWindowID iWinID) :
wxAuiNotebook(pclsParent, iWinID, wxDefaultPosition, pclsParent->GetClientSize(), wxAUI_NB_DEFAULT_STYLE)
{

}

sptWindowMgr::~sptWindowMgr()
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	wxAuiNotebook::DeleteAllPages();
}

bool sptWindowMgr::AddWindow(sptChildWindowBase *pclsNewPage, bool bSelect, const wxBitmap& clsIcon)
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	bool					bReturn;

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	if(nullptr != pclsNewPage)
	{
		bReturn = wxAuiNotebook::AddPage(pclsNewPage, pclsNewPage->GetTitle(), bSelect, clsIcon);
	}
	else
	{
		bReturn = false;
	}

	return bReturn;
}

sptChildWindowBase* sptWindowMgr::GetWindow(size_t sPageIndex) const
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	sptChildWindowBase*		pclsWindow = nullptr;

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	if(wxAuiNotebook::GetPageCount() > 0)
	{
		pclsWindow = dynamic_cast<sptChildWindowBase*>(wxAuiNotebook::GetPage(sPageIndex));
	}
	return pclsWindow;
}

int sptWindowMgr::SelectWindow(size_t sPageIndex)
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	int						iPreviousSelection = wxNOT_FOUND;

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	if((wxAuiNotebook::GetPageCount() > 0) && (sPageIndex < wxAuiNotebook::GetPageCount()))
	{
		iPreviousSelection = wxAuiNotebook::SetSelection(sPageIndex);
	}
	return iPreviousSelection;
}

int sptWindowMgr::SelectWindow(sptChildWindowBase* pclsWindow)
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	int						iPreviousSelection = wxNOT_FOUND;
	int						iSelection = wxAuiNotebook::GetPageIndex(pclsWindow);

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	if(wxNOT_FOUND != iSelection)
	{
		iPreviousSelection = wxAuiNotebook::SetSelection(iSelection);
	}
	return iPreviousSelection;
}


sptChildWindowBase* sptWindowMgr::GetSelectedWindow(void)
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	sptChildWindowBase*		pclsWindow = nullptr;
	int						iSelection = wxAuiNotebook::GetSelection();

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	if(wxNOT_FOUND != iSelection)
	{
		pclsWindow = dynamic_cast<sptChildWindowBase*>(wxAuiNotebook::GetPage(iSelection));
	}

	return pclsWindow;
}

sptChildWindowBase* sptWindowMgr::SearchOpenedFile(const wxString& cstrFilePath)
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	sptChildWindowBase*		pclsWindow = nullptr;

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	for(size_t sWindowIdx=0; sWindowIdx<wxAuiNotebook::GetPageCount(); sWindowIdx++)
	{
		sptChildWindowBase* pclsSelectedWindow = GetWindow(sWindowIdx);
		if(pclsSelectedWindow->GetFilePath().IsSameAs(cstrFilePath, false))
		{
			pclsWindow = pclsSelectedWindow;
			break;
		}
	}

	return pclsWindow;
}

void sptWindowMgr::UpdateTitle(int iPageIndex)
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	sptChildWindowBase*		pclsWindow = dynamic_cast<sptChildWindowBase*>(wxAuiNotebook::GetPage(iPageIndex));;

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	if(nullptr != pclsWindow)
	{
		pclsWindow->UpdateTitle();
		wxAuiNotebook::SetPageText(iPageIndex, pclsWindow->GetTitle());
	}
}

