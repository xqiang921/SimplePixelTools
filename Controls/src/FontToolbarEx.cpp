//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include "FontToolbarEx.h"
#include "Settings.h"
#include "Debug.h"
#include <wx/fontenum.h>

//=======================================================================//
//= Event table.													    =//
//=======================================================================//
BEGIN_EVENT_TABLE(FontToolBarEx, ToolbarBase)
	EVT_TOOL	(FONT_TOOL_EX_ID_FORCE_WIDTH,		FontToolBarEx::OnToolEvent)
	EVT_TOOL	(FONT_TOOL_EX_ID_CUT_OUT,			FontToolBarEx::OnToolEvent)
	EVT_TOOL	(FONT_TOOL_EX_ID_OFFSET_LEFT,		FontToolBarEx::OnToolEvent)
	EVT_TOOL	(FONT_TOOL_EX_ID_OFFSET_UP,			FontToolBarEx::OnToolEvent)
	EVT_TOOL	(FONT_TOOL_EX_ID_OFFSET_RIGHT,		FontToolBarEx::OnToolEvent)
	EVT_TOOL	(FONT_TOOL_EX_ID_OFFSET_DOWN,		FontToolBarEx::OnToolEvent)
	EVT_TOOL	(FONT_TOOL_EX_ID_OFFSET_RESET,		FontToolBarEx::OnToolEvent)
	EVT_SPINCTRL(FONT_TOOL_EX_ID_FORCE_WIDTH_VALUE,	FontToolBarEx::OnSpinChanged)
	EVT_SPINCTRL(FONT_TOOL_EX_ID_CUT_OUT_HEIGHT,	FontToolBarEx::OnSpinChanged)
END_EVENT_TABLE()

//=======================================================================//
//= Function define.										            =//
//=======================================================================//
FontToolBarEx::FontToolBarEx(wxWindow* pclsParent, wxWindowID iID)
: ToolbarBase(pclsParent, iID)
{
	// Force width
	wxToolBar::AddTool(FONT_TOOL_EX_ID_FORCE_WIDTH, wxT("Force Width"), wxBITMAP_PNG(RES_ID_TOOL_IMAGE_FONT_FORCED_WIDTH), wxNullBitmap, wxITEM_CHECK, wxT("Force Width"), wxT("Using fixed width when convert."), nullptr);
	m_pclsForceWidthSpin = new wxSpinCtrl(this, FONT_TOOL_EX_ID_FORCE_WIDTH_VALUE, wxT("6"), wxDefaultPosition, wxSize(50, -1), wxSP_ARROW_KEYS, 4, 64, 6 );
	wxToolBar::AddControl(m_pclsForceWidthSpin);
	// Cut out
	wxToolBar::AddTool(FONT_TOOL_EX_ID_CUT_OUT, wxT("Cut Out"), wxBITMAP_PNG(RES_ID_TOOL_IMAGE_FONT_CUT_OUT), wxNullBitmap, wxITEM_CHECK, wxT("Force Out"), wxT("Cut out the font image."), nullptr);
	m_pclsCutoutHeightSpin = new wxSpinCtrl(this, FONT_TOOL_EX_ID_CUT_OUT_HEIGHT, wxT("12"), wxDefaultPosition, wxSize(50, -1), wxSP_ARROW_KEYS, 4, 64, 6 );
	wxToolBar::AddControl(m_pclsCutoutHeightSpin);
	// Offset
	wxToolBar::AddTool(FONT_TOOL_EX_ID_OFFSET_UP, wxT("Offset Up"), wxBITMAP_PNG(RES_ID_TOOL_IMAGE_FONT_OFFSET_UP), wxNullBitmap, wxITEM_NORMAL, wxT("Offset up"), wxT("Offset up by one pixel."), nullptr);
	wxToolBar::AddTool(FONT_TOOL_EX_ID_OFFSET_DOWN, wxT("Offset Down"), wxBITMAP_PNG(RES_ID_TOOL_IMAGE_FONT_OFFSET_DOWN), wxNullBitmap, wxITEM_NORMAL, wxT("Offset down"), wxT("Offset down by one pixel."), nullptr);
	wxToolBar::AddTool(FONT_TOOL_EX_ID_OFFSET_LEFT, wxT("Offset Left"), wxBITMAP_PNG(RES_ID_TOOL_IMAGE_FONT_OFFSET_LEFT), wxNullBitmap, wxITEM_NORMAL, wxT("Offset left"), wxT("Offset left by one pixel."), nullptr);
	wxToolBar::AddTool(FONT_TOOL_EX_ID_OFFSET_RIGHT, wxT("Offset Right"), wxBITMAP_PNG(RES_ID_TOOL_IMAGE_FONT_OFFSET_RIGHT), wxNullBitmap, wxITEM_NORMAL, wxT("Offset right"), wxT("Offset right by one pixel."), nullptr);
	wxToolBar::AddTool(FONT_TOOL_EX_ID_OFFSET_RESET, wxT("Reset Offset"), wxBITMAP_PNG(RES_ID_TOOL_IMAGE_FONT_OFFSET_RESET), wxNullBitmap, wxITEM_NORMAL, wxT("Reset Offset"), wxT("Reset all offset."), nullptr);
	// Load work settings.
	_loadSettings();

	wxToolBar::Realize();
}

FontToolBarEx::~FontToolBarEx(void)
{
	/* Do special nothing. */
}

void FontToolBarEx::_loadSettings(void)
{
	/* Load work settings. */
	ToolbarBase::SetToggleTool(FONT_TOOL_EX_ID_FORCE_WIDTH, WorkSettingsInstance().PaintFont().IsForceWidth());
	m_pclsForceWidthSpin->Enable(WorkSettingsInstance().PaintFont().IsForceWidth());
	m_pclsForceWidthSpin->SetValue(WorkSettingsInstance().PaintFont().GetForcedWidthValue());
	ToolbarBase::SetToggleTool(FONT_TOOL_EX_ID_CUT_OUT, WorkSettingsInstance().PaintFont().IsCutoutEnable());
	m_pclsCutoutHeightSpin->Enable(WorkSettingsInstance().PaintFont().IsCutoutEnable());
	m_pclsCutoutHeightSpin->SetValue(WorkSettingsInstance().PaintFont().GetCutoutHeight());
}

void FontToolBarEx::_postPaintFontUpdateEvent(void)
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	wxEvtHandler*			pclsParentEvtHandler = wxToolBar::GetParent();

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	if(pclsParentEvtHandler)
	{
		wxCommandEvent clsFontUpdateEvent(wxEVT_TOOL, wxToolBarBase::GetId());
		pclsParentEvtHandler->ProcessEvent(clsFontUpdateEvent);
	}
}

void FontToolBarEx::OnToolEvent(wxCommandEvent& clsEvent)
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	bool					bFontSettingsUpdate = true;

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	switch(clsEvent.GetId())
	{
	case FONT_TOOL_EX_ID_FORCE_WIDTH:
		{
			GlobalPaintFont().SetForceWidth(ToolbarBase::GetToolToggled(FONT_TOOL_EX_ID_FORCE_WIDTH));
			m_pclsForceWidthSpin->Enable(ToolbarBase::GetToolToggled(FONT_TOOL_EX_ID_FORCE_WIDTH));
			break;
		}
	case FONT_TOOL_EX_ID_CUT_OUT:
		{
			GlobalPaintFont().EnableCutout(ToolbarBase::GetToolToggled(FONT_TOOL_EX_ID_CUT_OUT));
			m_pclsCutoutHeightSpin->Enable(ToolbarBase::GetToolToggled(FONT_TOOL_EX_ID_CUT_OUT));
			break;
		}
	case FONT_TOOL_EX_ID_OFFSET_UP:
		{
			GlobalPaintFont().DecOffsetY();
			break;
		}
	case FONT_TOOL_EX_ID_OFFSET_DOWN:
		{
			GlobalPaintFont().IncOffsetY();
			break;
		}
	case FONT_TOOL_EX_ID_OFFSET_LEFT:
		{
			GlobalPaintFont().DecOffsetX();
			break;
		}
	case FONT_TOOL_EX_ID_OFFSET_RIGHT:
		{
			GlobalPaintFont().IncOffsetX();
			break;
		}
	case FONT_TOOL_EX_ID_OFFSET_RESET:
		{
			GlobalPaintFont().SetOffsetX(0);
			GlobalPaintFont().SetOffsetY(0);
			break;
		}
	default:
		{
			bFontSettingsUpdate = false;
			WRN_LOG("Ignore event(%d) process.", clsEvent.GetId());
		}
	}

	if(bFontSettingsUpdate)
	{
		_postPaintFontUpdateEvent();
	}
	clsEvent.Skip();
}

void FontToolBarEx::OnSpinChanged(wxSpinEvent& clsEvent)
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	bool					bFontSettingsUpdate = true;

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	switch(clsEvent.GetId())
	{
	case FONT_TOOL_EX_ID_FORCE_WIDTH_VALUE:
		{
			GlobalPaintFont().SetForcedWidthValue(m_pclsForceWidthSpin->GetValue());
			break;
		}
	case FONT_TOOL_EX_ID_CUT_OUT_HEIGHT:
		{
			GlobalPaintFont().SetCutoutHeight(m_pclsCutoutHeightSpin->GetValue());
			break;
		}
	default:
		{
			bFontSettingsUpdate = false;
			WRN_LOG("Ignore event(%d) process.", clsEvent.GetId());
		}
	}

	if(bFontSettingsUpdate)
	{
		_postPaintFontUpdateEvent();
	}
	clsEvent.Skip();
}

void FontToolBarEx::OnProcessStart(void)
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	wxToolBar::Enable(false);
	wxToolBar::Realize();
}

void FontToolBarEx::OnProcessStop(void)
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	wxToolBar::Enable(true);
	wxToolBar::Realize();
}
