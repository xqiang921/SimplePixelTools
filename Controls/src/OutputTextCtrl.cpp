//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include "OutputTextCtrl.h"
#include "Settings.h"

//=======================================================================//
//= Function define.										            =//
//=======================================================================//
OutputTextCtrl::OutputTextCtrl(wxWindow *pclsParent, wxWindowID iID)
: wxTextCtrl(pclsParent, iID, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE|wxTE_READONLY, wxDefaultValidator, wxT("Output Text"))
{
	SetMinSize(wxSize(800, 200));
}

OutputTextCtrl::~OutputTextCtrl(void)
{

}

void OutputTextCtrl::AppendData(const RasterizedMonoImage& clsData)
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	wxString				strAppendTextContent;

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	/* Append data to list. */
	m_clsRasterizedDataArray.push_back(clsData);

	/* Index and character content comment. */
	if(BEFORE_CODE == GlobalOutputSettings().GetCommentType())
	{
		/* Add comment */
	}
	/* Character encode. */
	for(size_t sDataIndex=0; sDataIndex<clsData.Size(); sDataIndex++)
	{
		strAppendTextContent.Append(wxString::Format(wxT("0x%02X, "), clsData.Item(sDataIndex)));
		if((sDataIndex > 0) && (0 == ((sDataIndex+1) % 16)))
		{
			strAppendTextContent.Append('\n');
		}
	}
	/* Index and character content comment. */
	if(AFTER_CODE == GlobalOutputSettings().GetCommentType())
	{
		/* Add comment */
	}
	strAppendTextContent.Append('\n');
	/* Append character convert content to text box control. */
	wxTextCtrl::AppendText(strAppendTextContent);
}

void OutputTextCtrl::AppendData(const RasterizedMonoChar& clsData)
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	wxString			strAppendTextContent;

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	/* Append data to list. */
	m_clsRasterizedDataArray.push_back(clsData);

	/* Index and character content comment. */
	if(BEFORE_CODE == GlobalOutputSettings().GetCommentType())
	{
		strAppendTextContent.Append(clsData.GetComment());
		if(true == GlobalOutputSettings().GetCommentInNewline())
		{
			strAppendTextContent.Append('\n');
		}
	}
	/* Character encode. */
	for(size_t sDataIndex=0; sDataIndex<clsData.Size(); sDataIndex++)
	{
		strAppendTextContent.Append(wxString::Format(wxT("0x%02X, "), clsData.Item(sDataIndex)));
	}
	/* Index and character content comment. */
	if(AFTER_CODE == GlobalOutputSettings().GetCommentType())
	{
		if(true == GlobalOutputSettings().GetCommentInNewline())
		{
			strAppendTextContent.Append('\n');
		}
		strAppendTextContent.Append(clsData.GetComment());
	}
	strAppendTextContent.Append('\n');
	/* Append character convert content to text box control. */
	wxTextCtrl::AppendText(strAppendTextContent);
}

void OutputTextCtrl::Clear(void)
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	m_clsRasterizedDataArray.clear();
	m_clsRasterizedDataArray.resize(0);
	wxTextCtrl::Clear();
}

bool OutputTextCtrl::SaveAsTextFile(const wxString& cstrPath)
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	bool				bReturn = true;

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	bReturn = wxTextCtrl::SaveFile(cstrPath);

	return bReturn;
}

bool OutputTextCtrl::SaveAsBinaryFile(const wxString& cstrPath)
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	bool				bReturn = true;
	wxFile				clsBinaryFile(cstrPath, wxFile::write);

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	if(clsBinaryFile.IsOpened())
	{
		/* Loop for each character. */
		for(RasterizedDataArray::iterator clsIter = m_clsRasterizedDataArray.begin(); clsIter != m_clsRasterizedDataArray.end(); clsIter++)
		{
			/* Loop for each byte in rasterized data. */
			const RasterizedMonoData& clsRasterized = (*clsIter).Data();
			for(RasterizedMonoData::const_iterator clsByteIter = clsRasterized.begin(); clsByteIter != clsRasterized.end(); clsByteIter++)
			{
				bReturn = clsBinaryFile.Write(clsByteIter, sizeof(uint8_t));
				if(false == bReturn)
				{
					/* Write error while writing. */
					break;
				}
			}
		}
	}
	else
	{
		/* open file failed. */
		bReturn = false;
	}

	return bReturn;
}

bool OutputTextCtrl::IsEmpty(void) const
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	return (wxTextCtrl::IsEmpty() || m_clsRasterizedDataArray.empty());
}
